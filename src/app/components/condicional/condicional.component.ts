import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {

  persona: any = {
    nombre: 'Harry Styles',
    datos: 'Edad: 28 años   Altura: 1.83',
  }
  mostrar: boolean = true;
  
  constructor() { }

  ngOnInit(): void {
  }

}
